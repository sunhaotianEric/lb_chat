package com.lb.provider;

import com.lb.chat.model.vo.UserGroup;

import java.util.List;

public interface GroupProvider {


    String listGroupByUserId(Integer userid);;

    String updateMemberIdentity(Integer userId,Integer groupid);
}
