/**
 *
 */
package com.lb.chat.processor;

import com.lb.SpringUtil;
import com.lb.chat.model.vo.UserGroup;
import com.lb.chat.service.group.GroupService;
import com.lb.chat.service.user.UserService;
import org.jim.common.ImConst;
import org.jim.common.ImSessionContext;
import org.jim.common.ImStatus;
import org.jim.common.cache.redis.RedisCache;
import org.jim.common.cache.redis.RedisCacheManager;
import org.jim.common.packets.*;
import org.jim.server.command.handler.processor.login.LoginCmdProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.tio.core.ChannelContext;

import java.util.*;

/**
 * @author WChao
 *
 */
@Component
public class LoginServiceProcessor implements LoginCmdProcessor {

    private Logger logger = LoggerFactory.getLogger(LoginServiceProcessor.class);

    RedisCache groupCache = RedisCacheManager.register(ImConst.GROUP, Integer.MAX_VALUE, Integer.MAX_VALUE);


    protected void initGroupIdentity(String userid,String Groupid,Integer identity){
        if(identity==1 ||identity==2){
            List<String> manager=groupCache.listGetAll(Groupid + ":" + ImConst.MANAGER);
            if(manager==null){
                manager=new ArrayList<>();
            }
            if (!manager.contains(userid)) {
                groupCache.listPushTail(Groupid + ":" + ImConst.MANAGER, userid);
            }
        }

    }


    public List<Group> initGroups(User user) {
        // 群组;正式根据业务去查数据库或者缓存;
        GroupService groupService=SpringUtil.getBean(GroupService.class);
        List<UserGroup> userGroupList=groupService.listGroupByUserId(Integer.valueOf(user.getId()));
        List<Group> list=new ArrayList<>();
        for (int i=0;i<userGroupList.size();i++){
            Group group=new Group();
            group.setAvatar(userGroupList.get(i).getAvatar());
            group.setGroup_id(userGroupList.get(i).getGroupId().toString());
            group.setName(userGroupList.get(i).getGroupName());
            list.add(group);
            this.initGroupIdentity(user.getId(),group.getGroup_id(),userGroupList.get(i).getRole());
        }
        return list;
    }

    public List<Group> initFriends(User user) {
        List<Group> friends = new ArrayList<Group>();
        Group myFriend = new Group("0", "我的好友");
        List<User> myFriendGroupUsers = new LinkedList<>();

        myFriend.setUsers(myFriendGroupUsers);
        friends.add(myFriend);
        return friends;
    }


    public String newToken() {
        return UUID.randomUUID().toString();
    }

    /**
     * doLogin方法注意：J-IM登陆命令是根据user是否为空判断是否登陆成功,
     *
     * 当登陆失败时设置user属性需要为空，相反登陆成功user属性是必须非空的;
     */
    @Override
    public LoginRespBody doLogin(LoginReqBody loginReqBody, ChannelContext channelContext) {
        UserService userService= SpringUtil.getBean(UserService.class);
        String token = loginReqBody.getToken();
        User user = userService.checkLogin(token);
        LoginRespBody loginRespBody;
        if (user == null) {
            loginRespBody = new LoginRespBody(Command.COMMAND_LOGIN_RESP, ImStatus.C10008);
        } else {
            user.setGroups(initGroups(user));
            user.setFriends(initFriends(user));
            loginRespBody = new LoginRespBody(Command.COMMAND_LOGIN_RESP, ImStatus.C10007, user);
        }
        return loginRespBody;
    }

    @Override
    public void onSuccess(ChannelContext channelContext) {
        logger.info("回调方法");
        ImSessionContext imSessionContext = (ImSessionContext) channelContext.getAttribute();
        User user = imSessionContext.getClient().getUser();
	/*	if (user.getGroups() != null) {
			for (Group group : user.getGroups()) {// 发送加入群组通知
				ImPacket groupPacket = new ImPacket(Command.COMMAND_JOIN_GROUP_REQ, JsonKit.toJsonBytes(group));
				try {
					JoinGroupReqHandler joinGroupReqHandler = CommandManager.getCommand(Command.COMMAND_JOIN_GROUP_REQ,
							JoinGroupReqHandler.class);
					joinGroupReqHandler.joinGroupNotify(groupPacket, channelContext);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}*/
    }

    @Override
    public boolean isProtocol(ChannelContext channelContext) {

        return true;
    }

    @Override
    public String name() {

        return "default";
    }
}
