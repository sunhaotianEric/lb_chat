package com.lb.chat.processor;

import com.alibaba.fastjson.JSONObject;
import com.lb.SpringUtil;
import com.lb.chat.service.group.GroupService;
import com.lb.chat.service.user.UserService;
import org.jim.common.ImAio;
import org.jim.common.ImConst;
import org.jim.common.ImPacket;
import org.jim.common.cache.redis.RedisCache;
import org.jim.common.cache.redis.RedisCacheManager;
import org.jim.common.packets.ChangeManagerRespBody;
import org.jim.common.packets.Command;
import org.jim.common.packets.User;
import org.jim.server.command.handler.processor.flushuser.FlushUserServiceProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.tio.core.ChannelContext;

import java.util.List;

public class FlushUserProcessor implements FlushUserServiceProcessor {

    RedisCache userCache = RedisCacheManager.register(ImConst.USER, Integer.MAX_VALUE, Integer.MAX_VALUE);

    RedisCache groupCache = RedisCacheManager.register(ImConst.GROUP, Integer.MAX_VALUE, Integer.MAX_VALUE);

    UserService userService= SpringUtil.getBean(UserService.class);

    GroupService groupService=SpringUtil.getBean(GroupService.class);


    @Override
    public User flushUser(String id) {
        User user=userService.flushUser(Integer.valueOf(id));
        userCache.put(id+":"+"info", user);
        return user;
    }

    @Override
    public String changeManager(String userid, String groupid) {
        JSONObject jsonObject=groupService.changeManager(Integer.valueOf(userid),Integer.valueOf(groupid));
        if(jsonObject==null){
            return null;
        }
        ChangeManagerRespBody body=new ChangeManagerRespBody();
        body.setCommand(Command.COMMAND_GROUPUPDATEAGENT_RESP);
        body.setUserId(String.valueOf(jsonObject.get("memberId")));
        body.setGroupId(String.valueOf(jsonObject.get("groupId")));
        body.setIdentity((Integer)jsonObject.get("identity"));
        //更新缓存
        List<String> manager=groupCache.listGetAll(body.getGroupId() + ":" + ImConst.MANAGER);
        if(body.getIdentity().equals(2)){
            if (!manager.contains(body.getUserId())){
                groupCache.listPushTail(body.getGroupId() + ":" + ImConst.MANAGER, body.getUserId());
            }
        }else {
            if (manager.contains(body.getUserId())){
                groupCache.listRemove(body.getGroupId() + ":" + ImConst.MANAGER, body.getUserId());
            }
        }
        //发送通知给群所有人
        ImPacket imPacket=new ImPacket(Command.COMMAND_GROUPUPDATEAGENT_RESP,body.toByte());
        ImAio.sendToGroup(groupid,imPacket);
        return null;
    }

    @Override
    public boolean isProtocol(ChannelContext channelContext) {
        return true;
    }

    @Override
    public String name() {
        return "flush";
    }
}
