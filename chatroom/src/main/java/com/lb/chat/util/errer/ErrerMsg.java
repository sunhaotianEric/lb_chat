package com.lb.chat.util.errer;

import lombok.Getter;

/**
 *   错误返回定义
 */
public enum ErrerMsg {

    ERRER100(100,"Request exception"),

    ERRER420(420,"Parameter error"),

    ERRER600(600," Database update exception");

   @Getter
   private int code ;

   @Getter
   private String message;

    ErrerMsg(int code , String message){
        this.code=code;
        this.message=message;
    }

}
