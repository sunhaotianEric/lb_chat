package com.lb.chat.model.user;


import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class User implements Serializable {

    Integer id;

    String accountName;

    String password;

    Integer phone;

    String name;

    Integer sex;

    Integer age;

    String profilePicture;

    Date createTime;

    Integer isForbidden;

    Date loginTime;

    Integer isCheckPhone;

    Double goldCoin;

    Integer groupRole; // 标识用户所在群组角色 1:群主 2:管理员 3:成员 4:游客
}
