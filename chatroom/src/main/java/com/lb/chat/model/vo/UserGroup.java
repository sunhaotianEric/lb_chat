package com.lb.chat.model.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserGroup implements Serializable {

    Integer userId;

    Integer groupId;

    String nick;

    Integer role;

    String groupName;

    String avatar;


}
