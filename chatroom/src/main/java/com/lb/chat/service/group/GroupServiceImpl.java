package com.lb.chat.service.group;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lb.chat.model.vo.UserGroup;
import com.lb.provider.GroupProvider;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupServiceImpl implements GroupService {

    @Reference(version = "1.0.0")
    GroupProvider groupProvider;

    @Override
    public List<UserGroup> listGroupByUserId(Integer userid) {
        return JSON.parseArray(groupProvider.listGroupByUserId(userid),UserGroup.class);
    }

    @Override
    public JSONObject changeManager(Integer userid, Integer groupid) {
        String json=groupProvider.updateMemberIdentity(userid,groupid);
        if(json==null){
            return null;
        }
        return JSON.parseObject(json);
    }
}
