package com.lb.chat.service.group;

import com.alibaba.fastjson.JSONObject;
import com.lb.chat.model.vo.UserGroup;

import java.util.List;

public interface GroupService {

    List<UserGroup> listGroupByUserId(Integer userid);

    JSONObject changeManager(Integer userid, Integer groupid);
}
