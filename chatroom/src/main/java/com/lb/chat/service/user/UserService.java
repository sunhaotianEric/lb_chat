package com.lb.chat.service.user;

import org.jim.common.packets.User;

public interface UserService {

    User checkLogin(String token);

    User flushUser(Integer id);


}
