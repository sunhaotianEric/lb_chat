package com.lb.chat.service.user;

import com.alibaba.dubbo.config.annotation.Reference;
import com.lb.provider.UserProvider;
import org.jim.common.packets.User;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Reference(version = "1.0.0")
    UserProvider userProvider;

    @Override
    public User checkLogin(String token) {
        com.lb.chat.model.user.User user=userProvider.checkLogin(token);
        User jimUser=new User();
        jimUser.setId(user.getId().toString());
        jimUser.setAccount(user.getAccountName());
        jimUser.setNick(user.getName());
        jimUser.setAvatar(user.getProfilePicture());
        if (1 == user.getGroupRole() || 2 == user.getGroupRole()){
            jimUser.setUserType(2);
        }else if (3 == user.getGroupRole()){ // 标识用户所在群组角色 1:群主 2:管理员 3:成员 4:游客
            jimUser.setUserType(1);
        }else{
            jimUser.setUserType(0); // 用户类型  0:游客   1:会员   2:管理员
        }
        jimUser.setTerminal("ws");
        return jimUser;
//        if (1 == user.getIsVisitor()){
//            jimUser.setUserType(0); // 用户类型  0:游客   1:会员   2:管理员
//        }else{
//            jimUser.setUserType(1); // 暂标记为1:会员
//        }
    }

    @Override
    public User flushUser(Integer id) {
        com.lb.chat.model.user.User user=userProvider.flushUser(id);
        User jimUser=new User();
        jimUser.setId(user.getId().toString());
        jimUser.setAvatar(user.getProfilePicture());
        jimUser.setNick(user.getName());
        jimUser.setTerminal("ws");
        return jimUser;
    }


}
