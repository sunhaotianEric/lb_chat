package com.lb;

import com.lb.chat.processor.FlushUserProcessor;
import com.lb.chat.processor.LoginServiceProcessor;
import org.jim.common.ImConfig;
import org.jim.common.config.PropertyImConfigBuilder;
import org.jim.common.packets.Command;
import org.jim.server.ImServerStarter;
import org.jim.server.command.CommandManager;
import org.jim.server.command.handler.FlushUserReqHandler;
import org.jim.server.command.handler.HandshakeReqHandler;
import org.jim.server.command.handler.LoginReqHandler;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import com.lb.chat.command.DemoWsHandshakeProcessor;
import com.lb.chat.listener.ImDemoGroupListener;


@Component
public class ServerRunner implements CommandLineRunner {

	public static ImConfig imConfig;
//	@Value
	String jimPropertiesPath;
	
    @Override
    public void run(String... args) throws Exception {

    	imConfig = new PropertyImConfigBuilder("jim.properties").build();
		//初始化SSL;(开启SSL之前,你要保证你有SSL证书哦...)
		//initSsl(imConfig);
		//设置群组监听器，非必须，根据需要自己选择性实现;
		imConfig.setImGroupListener(new ImDemoGroupListener());
		ImServerStarter imServerStarter = new ImServerStarter(imConfig);
		/*****************start 以下处理器根据业务需要自行添加与扩展，每个Command都可以添加扩展,此处为demo中处理**********************************/
		HandshakeReqHandler handshakeReqHandler = CommandManager.getCommand(Command.COMMAND_HANDSHAKE_REQ, HandshakeReqHandler.class);
		//添加自定义握手处理器;
		handshakeReqHandler.addProcessor(new DemoWsHandshakeProcessor());
		LoginReqHandler loginReqHandler = CommandManager.getCommand(Command.COMMAND_LOGIN_REQ,LoginReqHandler.class);
		//添加登录业务处理器;
		loginReqHandler.addProcessor(new LoginServiceProcessor());

		FlushUserReqHandler flushUserReqHandler =CommandManager.getCommand(Command.COMMAND_FLUSHUSER_REQ, FlushUserReqHandler.class);
		flushUserReqHandler.addProcessor(new FlushUserProcessor());

//		ChatReqHandler chatReqHandler = CommandManager.getCommand(Command.COMMAND_CHAT_REQ,ChatReqHandler.class);
//		chatReqHandler.addProcessor(new ChatMessageProcessor());
		/*****************end *******************************************************************************************/
		imServerStarter.start();
    }

	/**
	 * 开启SSL之前，你要保证你有SSL证书哦！
	 * @param imConfig
	 * @throws Exception
	 */
/*	private static void initSsl(ImConfig imConfig) throws Exception {
		//开启SSL
		if(ImConst.ON.equals(imConfig.getIsSSL())){
			String keyStorePath = PropKit.get("jim.key.store.path");
			String keyStoreFile = keyStorePath;
			String trustStoreFile = keyStorePath;
			String keyStorePwd = PropKit.get("jim.key.store.pwd");
			if (StringUtils.isNotBlank(keyStoreFile) && StringUtils.isNotBlank(trustStoreFile)) {
				SslConfig sslConfig = SslConfig.forServer(keyStoreFile, trustStoreFile, keyStorePwd);
				imConfig.setSslConfig(sslConfig);
			}
		}
	}*/
}
