/**
 * 
 */
package org.jim.common.packets;

import com.alibaba.fastjson.JSONObject;

/**
 * 版本: [1.0]
 * 功能说明: 
 * 作者: WChao 创建时间: 2017年7月26日 上午11:34:44
 */
public class BannedBody extends Message {
	
	private static final long serialVersionUID = 5731474214655476286L;
	/**
	 * 发送用户id;
	 */
	private String from;
	
	private String groupId;
	
	private Integer bannedInd;
	
	public Integer getBannedInd() {
		return bannedInd;
	}



	public void setBannedInd(Integer bannedInd) {
		this.bannedInd = bannedInd;
	}



	private BannedBody(){}
	
	
	public String getFrom() {
		return from;
	}
	public BannedBody setFrom(String from) {
		this.from = from;
		return this;
	}



	public String getGroupId() {
		return groupId;
	}



	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	
	
	
	

}
