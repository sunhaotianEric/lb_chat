package org.jim.common.packets;

/**
 * @author dc
 * @title: HasFriendReqBody
 * @projectName nb_chat_core
 * @description: TODO
 * @date 2019/7/1010:18
 */
public class HasFriendReqBody extends Message {

    private static final long serialVersionUID = 6334331164604259933L;

    private String friendId;


    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }
}
