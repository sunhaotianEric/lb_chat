package org.jim.common.packets;

/**
 * @author dc
 * @title: OutGroupBody
 * @projectName nb_chat_core
 * @description: TODO
 * @date 2019/6/2815:27
 */
public class OutGroupBody  extends RespBody {

    private static final long serialVersionUID = 7615640195656454389L;


    private String userid;

    private Group group;

    private String explain;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    public OutGroupBody(Command command, String userid, Group group, String explain) {
        super(command);
        this.userid = userid;
        this.group = group;
        this.explain = explain;
    }
}
