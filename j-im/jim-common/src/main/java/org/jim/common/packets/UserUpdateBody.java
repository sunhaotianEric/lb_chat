package org.jim.common.packets;

/**
 * @author dc
 * @title: UserUpdateBody
 * @projectName nb_chat_core
 * @description: TODO
 * @date 2019/7/915:23
 */
public class UserUpdateBody extends RespBody {

    String explain;

    String userId;

    public UserUpdateBody(Command command, String explain, String userId) {
        super(command);
        this.explain = explain;
        this.userId = userId;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
