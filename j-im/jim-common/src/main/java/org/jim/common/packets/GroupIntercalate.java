package org.jim.common.packets;

/**
 * @author dc
 * @title: GroupIntercalate
 * @projectName nb_chat_core
 * @description: TODO
 * @date 2019/6/2218:01
 */
public class GroupIntercalate {

    private static final long serialVersionUID = 54234801L;

    private String groupId;

    private Integer isBanned;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Integer getIsBanned() {
        return isBanned;
    }

    public void setIsBanned(Integer isBanned) {
        this.isBanned = isBanned;
    }
}
