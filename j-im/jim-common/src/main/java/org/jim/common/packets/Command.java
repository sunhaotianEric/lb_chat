package org.jim.common.packets;

import org.jim.common.utils.DynamicEnumUtil;

public enum Command{
  /**
   * <code>COMMAND_UNKNOW = 0;</code>
   */
  COMMAND_UNKNOW(0),
  /**
   * <pre>
   *握手请求，含http的websocket握手请求
   * </pre>
   *
   * <code>COMMAND_HANDSHAKE_REQ = 1;</code>
   */
  COMMAND_HANDSHAKE_REQ(1),
  /**
   * <pre>
   *握手响应，含http的websocket握手响应
   * </pre>
   *
   * <code>COMMAND_HANDSHAKE_RESP = 2;</code>
   */
  COMMAND_HANDSHAKE_RESP(2),
  /**
   * <pre>
   *鉴权请求
   * </pre>
   *
   * <code>COMMAND_AUTH_REQ = 3;</code>
   */
  COMMAND_AUTH_REQ(3),
  /**
   * <pre>
   * 鉴权响应
   * </pre>
   *
   * <code>COMMAND_AUTH_RESP = 4;</code>
   */
  COMMAND_AUTH_RESP(4),
  /**
   * <pre>
   *登录请求
   * </pre>
   *
   * <code>COMMAND_LOGIN_REQ = 5;</code>
   */
  COMMAND_LOGIN_REQ(5),
  /**
   * <pre>
   *登录响应
   * </pre>
   *
   * <code>COMMAND_LOGIN_RESP = 6;</code>
   */
  COMMAND_LOGIN_RESP(6),
  /**
   * <pre>
   *进入群组请求
   * </pre>
   *
   * <code>COMMAND_JOIN_GROUP_REQ = 7;</code>
   */
  COMMAND_JOIN_GROUP_REQ(7),
  /**
   * <pre>
   *进入群组响应
   * </pre>
   *
   * <code>COMMAND_JOIN_GROUP_RESP = 8;</code>
   */
  COMMAND_JOIN_GROUP_RESP(8),
  /**
   * <pre>
   *进入群组通知
   * </pre>
   *
   * <code>COMMAND_JOIN_GROUP_NOTIFY_RESP = 9;</code>
   */
  COMMAND_JOIN_GROUP_NOTIFY_RESP(9),
  /**
   * <pre>
   *退出群组通知
   * </pre>
   *
   * <code>COMMAND_EXIT_GROUP_NOTIFY_RESP = 10;</code>
   */
  COMMAND_EXIT_GROUP_NOTIFY_RESP(10),
  /**
   * <pre>
   *聊天请求
   * </pre>
   *
   * <code>COMMAND_CHAT_REQ = 11;</code>
   */
  COMMAND_CHAT_REQ(11),
  /**
   * <pre>
   *聊天响应
   * </pre>
   *
   * <code>COMMAND_CHAT_RESP = 12;</code>
   */
  COMMAND_CHAT_RESP(12),
  /**
   * <pre>
   *心跳请求
   * </pre>
   *
   * <code>COMMAND_HEARTBEAT_REQ = 13;</code>
   */
  COMMAND_HEARTBEAT_REQ(13),
  /**
   * <pre>
   *关闭请求
   * </pre>
   *
   * <code>COMMAND_CLOSE_REQ = 14;</code>
   */
  COMMAND_CLOSE_REQ(14),
  /**
   * <pre>
   *发出撤消消息指令(管理员可以撤消所有人的消息，自己可以撤消自己的消息)
   * </pre>
   *
   * <code>COMMAND_CANCEL_MSG_REQ = 15;</code>
   */
  COMMAND_CANCEL_MSG_REQ(15),
  /**
   * <pre>
   *收到撤消消息指令
   * </pre>
   *
   * <code>COMMAND_CANCEL_MSG_RESP = 16;</code>
   */
  COMMAND_CANCEL_MSG_RESP(16),
  /**
   * <pre>
   *获取用户信息;
   * </pre>
   *
   * <code>COMMAND_GET_USER_REQ = 17;</code>
   */
  COMMAND_GET_USER_REQ(17),
  /**
   * <pre>
   *获取用户信息响应;
   * </pre>
   *
   * <code>COMMAND_GET_USER_RESP = 18;</code>
   */
  COMMAND_GET_USER_RESP(18),
  /**
   * <pre>
   * 获取聊天消息;
   * </pre>
   *
   * <code>COMMAND_GET_MESSAGE_REQ = 19;</code>
   */
  COMMAND_GET_MESSAGE_REQ(19),
  /**
   * <pre>
   * 获取聊天消息响应;
   * </pre>
   *
   * <code>COMMAND_GET_MESSAGE_RESP = 20;</code>
   */
  COMMAND_GET_MESSAGE_RESP(20),

  /**
   * <pre>
   * 禁言设置请求;
   * </pre>
   *
   * <code>COMMAND_BANNED_REQ = 21;</code>
   */
  COMMAND_BANNED_REQ(21),

  /**
   * <pre>
   * 禁言设置(禁言状态)响应;
   * </pre>
   *
   * <code>COMMAND_BANNED_RESP = 22;</code>
   */
  COMMAND_BANNED_RESP(22),


  /**
   * <pre>
   * 好友请求;
   * </pre>
   *
   * <code>COMMAND_FRIEND_REQ = 23;</code>
   */
  COMMAND_FRIEND_REQ(23),

  /**
   * <pre>
   * 好友响应;
   * </pre>
   *
   * <code>COMMAND_FRIEND_RESP = 24;</code>
   */
  COMMAND_FRIEND_RESP(24),

  /**
   * 申请好友处理操作http响应
   */
  COMMAND_ADDFRIEND_HTTP_RESP(25),

  /**
   * 申请好友处理操作请求
   */
  COMMAND_ADDFRIEND_REQ(26),
  /**
   * 申请好友处理操作响应
   */
  COMMAND_ADDFRIEND_RESP(27),

  //删除好友操作响应
  COMMAND_DELFRIEND_RESP(28),

  //进入群组响应
  COMMAND_INSGROUP_RESP(29),

  //退出群组响应
  COMMAND_OUTGROUP_RESP(30),

  //申请进群响应
  COMMAND_APPLYGROUP_RESP(31),

  //更改群公告响应
  COMMAND_GROUPDESCRIPTION_RESP(32),

  //更改群管理员响应
  COMMAND_GROUPUPDATEAGENT_RESP(33),

  //用户修改信息通知
  COMMAND_USERUPDATE_RESP(34),

  //判断是否好友的请求
  COMMAND_HASFRIEND_REQ(35),

  //判断是否好友的响应
  COMMAND_HASFRIEND_RESP(36),

  //用户下线响应
  COMMAND_USEROFFLINE_RESP(37),

  //用户信息更新
  COMMAND_FLUSHUSER_REQ(38),
  //用户信息更新响应
  COMMAND_FLUSHUSER_RESP(39),

  //群聊审核验证请求
  COMMAND_CHATCHECKING_REQ(40),
  //群聊审核验证响应
  COMMAND_CHATCHECKING_RESP(41),
  //群聊审核结果请求
  COMMAND_CHATCHECKED_REQ(42),
  //群聊审核结果响应
  COMMAND_CHATCHECKED_RESP(43),
  //获取历史群聊审核消息请求
  COMMAND_CHATCHECKHIS_REQ(44),
  //获取历史群聊审核消息响应
  COMMAND_CHATCHECKHIS_RESP(45),
  //禁言状态请求
  COMMAND_BANNEDSTATE_REQ(46),
  //禁言状态响应
  COMMAND_BANNEDSTATE_RESP(47),
  //预留请求
  COMMAND_RESERVED_REQ(48),
  //预留响应
  COMMAND_RESERVED_RESP(49),
  //广播请求
  COMMAND_BROADCAST_REQ(50),
  //广播响应
  COMMAND_BROADCAST_RESP(51)
  ;


  public final int getNumber() {
    return value;
  }

  public static Command valueOf(int value) {
    return forNumber(value);
  }

  public static Command forNumber(int value) {
	  for(Command command : Command.values()){
	   	   if(command.getNumber() == value){
	   		   return command;
	   	   }
      }
	  return null;
  }

  public static Command addAndGet(String name , int value){
	  return DynamicEnumUtil.addEnum(Command.class, name,new Class[]{int.class}, new Object[]{value});
  }

  private final int value;

  private Command(int value) {
    this.value = value;
  }
}

