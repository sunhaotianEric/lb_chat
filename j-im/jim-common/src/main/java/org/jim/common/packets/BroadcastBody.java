/**
 *
 */
package org.jim.common.packets;

/**
 * 版本: [1.0]
 * 功能说明:
 * 作者: Alex 创建时间: 2019年10月31日 上午11:54:32
 */
public class BroadcastBody extends Message {

    private static final long serialVersionUID = -5687459633884615894L;
    /**
     * 发送用户id;
     */
    private String from;
    /**
     * 群组id;
     */
    private String groupId; // 为空时则为全站广播
    /**
     * 消息内容;
     */
    private String content;

    private BroadcastBody() {
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "BroadcastBody{" +
                "from='" + from + '\'' +
                ", groupId='" + groupId + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
