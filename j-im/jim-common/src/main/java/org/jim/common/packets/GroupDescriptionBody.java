package org.jim.common.packets;

/**
 * @author dc
 * @title: GroupDescriptionBody
 * @projectName nb_chat_core
 * @description: TODO
 * @date 2019/6/2916:28
 */
public class GroupDescriptionBody extends RespBody {

    private static final long serialVersionUID = 89222146554546282L;

    private String groupId;

    private String description;

    private Long time;

    private String explain;

    public GroupDescriptionBody(Command command, String groupId, String description, Long time, String explain) {
        super(command);
        this.groupId = groupId;
        this.description = description;
        this.time = time;
        this.explain = explain;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
