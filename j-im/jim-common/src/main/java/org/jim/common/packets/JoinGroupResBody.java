/**
 * 
 */
package org.jim.common.packets;

/**
 * 版本: [1.0]
 * 功能说明: 进入群组通知消息体
 * 作者: WChao 创建时间: 2017年7月26日 下午5:14:04
 */
public class JoinGroupResBody extends Message{
	
	private static final long serialVersionUID = 3828976681110713803L;
	private String groupId;
	
	private Integer bannedInd = 0;
	//禁言標誌位  add by george
	public Integer getBannedInd() {
		return bannedInd;
	}
	public void setBannedInd(Integer bannedInd) {
		this.bannedInd = bannedInd;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
}
