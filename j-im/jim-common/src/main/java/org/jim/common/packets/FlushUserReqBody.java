package org.jim.common.packets;

import com.alibaba.fastjson.JSONObject;

public class FlushUserReqBody extends Message {

    Integer cmdType;

    //扩展字段
    JSONObject extras;

    @Override
    public JSONObject getExtras() {
        return extras;
    }

    @Override
    public void setExtras(JSONObject extras) {
        this.extras = extras;
    }

    public Integer getCmdType() {
        return cmdType;
    }

    public void setCmdType(Integer cmdType) {
        this.cmdType = cmdType;
    }
}
