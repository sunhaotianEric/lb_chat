package org.jim.common.packets;

/**
 * @author dc
 * @title: DelFriendRespBody
 * @projectName nb_chat_core
 * @description: TODO
 * @date 2019/6/2219:04
 */
public class DelFriendRespBody extends RespBody {

    private static final long serialVersionUID = 873178967878949023L;

    /**
     * 发送用户id;
     */
    private String from;

    //被发送的用户id
    private String to;

    //说明
    private String explain;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    public DelFriendRespBody(Command command, String from, String to, String explain) {
        super(command);
        this.from = from;
        this.to = to;
        this.explain = explain;
    }
}
