package org.jim.common.packets;

/**
 * @author dc
 * @title: InsGroupRespBody
 * @projectName nb_chat_core
 * @description: TODO
 * @date 2019/6/2716:10
 */
public class InsGroupRespBody extends RespBody {

    private static final long serialVersionUID = 7635620195652369689L;
    private String userid;

    private String userNick;

    private Group group;


    private String explain;

    public String getUserNick() {
        return userNick;
    }

    public void setUserNick(String userNick) {
        this.userNick = userNick;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public InsGroupRespBody(Command command, String userid, Group group, String explain,String userNick) {
        super(command);
        this.userid = userid;
        this.group = group;
        this.explain = explain;
        this.userNick=userNick;
    }
}
