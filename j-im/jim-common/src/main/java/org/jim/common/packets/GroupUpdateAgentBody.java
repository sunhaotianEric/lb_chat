package org.jim.common.packets;

/**
 * @author dc
 * @title: GroupUpdateAgentBody
 * @projectName nb_chat_core
 * @description: TODO
 * @date 2019/7/115:04
 */
public class GroupUpdateAgentBody extends RespBody {

    private static final long serialVersionUID = 544136234801L;

    private String userid;

    private String userNick;

    private String groupId;

    private String groupName;

    private String explain;

    private Long time;

    private Integer agentType;

    public GroupUpdateAgentBody(Command command, String userid, String userNick, String groupId, String groupName, String explain, Long time, Integer agentType) {
        super(command);
        this.userid = userid;
        this.userNick = userNick;
        this.groupId = groupId;
        this.groupName = groupName;
        this.explain = explain;
        this.time = time;
        this.agentType = agentType;
    }

    public String getUserNick() {
        return userNick;
    }

    public void setUserNick(String userNick) {
        this.userNick = userNick;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Integer getAgentType() {
        return agentType;
    }

    public void setAgentType(Integer agentType) {
        this.agentType = agentType;
    }
}
