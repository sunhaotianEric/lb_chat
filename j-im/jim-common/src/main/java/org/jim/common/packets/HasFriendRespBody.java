package org.jim.common.packets;

/**
 * @author dc
 * @title: HasFriendRespBody
 * @projectName nb_chat_core
 * @description: TODO
 * @date 2019/7/1010:30
 */
public class HasFriendRespBody extends RespBody {

    private static final long serialVersionUID = 2148923678401784L;

    //0代表没好友 1代表存在好友
    private int hasFriendType;

    private String FriendId;

    private String explain;

    public HasFriendRespBody(Command command, int hasFriendType, String friendId, String explain) {
        super(command);
        this.hasFriendType = hasFriendType;
        FriendId = friendId;
        this.explain = explain;
    }

    public int getHasFriendType() {
        return hasFriendType;
    }

    public void setHasFriendType(int hasFriendType) {
        this.hasFriendType = hasFriendType;
    }

    public String getFriendId() {
        return FriendId;
    }

    public void setFriendId(String friendId) {
        FriendId = friendId;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }
}
