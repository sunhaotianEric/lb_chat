package org.jim.common.cache.redis;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
/**
 * @author WChao
 * @date 2018年3月9日 上午1:09:03
 */
public class RedisConfiguration {

	public int getDb() {
		return db;
	}

	public void setDb(int db) {
		this.db = db;
	}
	private  int retryNum =100;
	private  int maxActive=200;
	private  int maxIdle=50;
	private  long maxWait=5000L;
	private  int timeout=2000;
	private  String auth;
	private  String host = "";
	private  int port= 0;
	private  int db = 0;
	private Long hismsgsize;

	public RedisConfiguration(){}

	public RedisConfiguration(Properties prop){
		this.retryNum = Integer.valueOf(prop.getProperty("jim.redis.retrynum", "100"));
		this.maxActive = Integer.valueOf(prop.getProperty("jim.redis.maxactive","200"));
		this.maxIdle =  Integer.valueOf(prop.getProperty("jim.redis.maxidle", "50"));
		this.maxWait =  Long.valueOf(prop.getProperty("jim.redis.maxwait","5000"));
		this.timeout =  Integer.valueOf(prop.getProperty("jim.redis.timeout", "2000"));
		this.auth =  prop.getProperty("jim.redis.auth",null);
		this.db =  Integer.valueOf(prop.getProperty("jim.redis.db","0"));
		if(StringUtils.isEmpty(auth)) {
			this.auth = null;
		}
		this.host = prop.getProperty("jim.redis.host","");
		this.port =  Integer.valueOf(prop.getProperty("jim.redis.port","0"));
		// 读取群组历史消息设置缓存大小 Alex20191109：借用这里读取配置文件中的聊天室其他设置信息
		this.hismsgsize =  Long.valueOf(prop.getProperty("chatsetting.historymsgsize","65536"));
	}
	public int getRetryNum() {
		return retryNum;
	}
	public void setRetryNum(int retryNum) {
		this.retryNum = retryNum;
	}
	public int getMaxActive() {
		return maxActive;
	}
	public void setMaxActive(int maxActive) {
		this.maxActive = maxActive;
	}
	public int getMaxIdle() {
		return maxIdle;
	}
	public void setMaxIdle(int maxIdle) {
		this.maxIdle = maxIdle;
	}
	public long getMaxWait() {
		return maxWait;
	}
	public void setMaxWait(long maxWait) {
		this.maxWait = maxWait;
	}
	public int getTimeout() {
		return timeout;
	}
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getAuth() {
		return auth;
	}
	public void setAuth(String auth) {
		this.auth = auth;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}

	public Long getHismsgsize() {
		return hismsgsize;
	}
	public void setHismsgsize(Long hismsgsize) {
		this.hismsgsize = hismsgsize;
	}
}
