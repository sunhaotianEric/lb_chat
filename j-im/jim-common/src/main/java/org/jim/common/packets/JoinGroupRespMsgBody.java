package org.jim.common.packets;

import java.util.Date;

/**
 * @author dc
 * @title: JoinGroupRespMsgBody
 * @projectName nb_chat_core
 * @description: TODO
 * @date 2019/6/2911:26
 */
public class JoinGroupRespMsgBody extends RespBody {


    private static final long serialVersionUID = 6641320192752338569L;

    private String userid;

    private String usernick;

    private String avatar;

    private String groupid;

    private Long time;

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsernick() {
        return usernick;
    }

    public void setUsernick(String usernick) {
        this.usernick = usernick;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }


    public JoinGroupRespMsgBody(Command command, String userid, String usernick, String avatar, String groupid, Long time) {
        super(command);
        this.userid = userid;
        this.usernick = usernick;
        this.avatar = avatar;
        this.groupid = groupid;
        this.time = time;
    }

    @Override
    public String toString() {
        return "JoinGroupRespMsgBody{" +
                "userid='" + userid + '\'' +
                ", usernick='" + usernick + '\'' +
                ", avatar='" + avatar + '\'' +
                ", groupid='" + groupid + '\'' +
                ", time=" + time +
                '}';
    }
}
