package org.jim.common.packets;

/**
 * @author dc
 * @title: ApplyJoinGroupBody
 * @projectName nb_chat_core
 * @description: TODO
 * @date 2019/6/2916:07
 */
public class ApplyJoinGroupBody extends RespBody {

    private static final long serialVersionUID = 2374423146554562232L;

    private String userId;
    private String groupId;
    private String userNick;
    private Integer sex;
    private String groupName;
    private Long applytime;
    private String explain;

    public ApplyJoinGroupBody(Command command, String userId, String groupId, String userNick, Integer sex, String groupName,Long applytime,String explain) {
        super(command);
        this.userId = userId;
        this.groupId = groupId;
        this.userNick = userNick;
        this.sex = sex;
        this.groupName=groupName;
        this.applytime=applytime;
        this.explain=explain;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    public Long getApplytime() {
        return applytime;
    }

    public void setApplytime(Long applytime) {
        this.applytime = applytime;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getUserNick() {
        return userNick;
    }

    public void setUserNick(String userNick) {
        this.userNick = userNick;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }
}
