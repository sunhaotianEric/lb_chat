package org.jim.common.packets;

/**
 * @author dc
 * @title: GroupBannedBody
 * @projectName nb_chat_core
 * @description: TODO
 * @date 2019/6/2213:45
 */
public class GroupBannedBody extends RespBody{

    private static final long serialVersionUID = 5237472146554546282L;

    //发送用户id;
    private String from;
    //发送用户的账号名
    private String userName;
    //dd发送的用户id
    private String to;
    //说明
    private String explain;
    //生成实体的时间戳
    private Long timeStamp;
    //群id
    private Integer groupId;
    //是否禁言
    private Integer isBanned;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getIsBanned() {
        return isBanned;
    }

    public void setIsBanned(Integer isBanned) {
        this.isBanned = isBanned;
    }

    public GroupBannedBody(Command command, String from, String userName, String to, String explain, Long timeStamp, Integer groupId, Integer isBanned) {
        super(command);
        this.from = from;
        this.userName = userName;
        this.to = to;
        this.explain = explain;
        this.timeStamp = timeStamp;
        this.groupId = groupId;
        this.isBanned = isBanned;
    }
}
