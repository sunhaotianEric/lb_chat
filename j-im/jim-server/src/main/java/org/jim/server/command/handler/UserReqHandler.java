/**
 *
 */
package org.jim.server.command.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.jim.common.*;
import org.jim.common.cache.redis.JedisTemplate;
import org.jim.common.cache.redis.RedisCache;
import org.jim.common.cache.redis.RedisCacheManager;
import org.jim.common.message.MessageHelper;
import org.jim.common.packets.Command;
import org.jim.common.packets.Group;
import org.jim.common.packets.RespBody;
import org.jim.common.packets.User;
import org.jim.common.packets.UserReqBody;
import org.jim.common.utils.ImKit;
import org.jim.common.utils.JsonKit;
import org.jim.server.command.AbstractCmdHandler;
import org.tio.core.ChannelContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 版本: [1.0]
 * 功能说明: 获取用户信息消息命令
 * @author : WChao 创建时间: 2017年9月18日 下午4:08:47
 */
public class UserReqHandler extends AbstractCmdHandler implements ImConst {

    RedisCache groupCache = RedisCacheManager.register(ImConst.GROUP, Integer.MAX_VALUE, Integer.MAX_VALUE);

    private final String SUBFIX = ":";

    @Override
    public ImPacket handler(ImPacket packet, ChannelContext channelContext) throws Exception {
        return doTemp(packet, channelContext);
//        return dohandler(packet, channelContext);
    }

    // Alex20191110：简化获取在线用户列表的业务实现最大限度提高性能。(因现聊天室只有一个默认群组groupid:1、前端页面除了请求“群组在线用户列表”暂无其他类型用户列表的请求)
    private ImPacket doTemp(ImPacket packet, ChannelContext channelContext) throws Exception{
//        long start, end;
//        start = System.currentTimeMillis();

        UserReqBody userReqBody = JsonKit.toBean(packet.getBody(), UserReqBody.class);
//        //(0:所有在线用户,1:所有离线用户,2:所有用户[在线+离线]);
//        Integer type = userReqBody.getType() == null ? 2 : userReqBody.getType();
        ImSessionContext imSessionContext = (ImSessionContext) channelContext.getAttribute();
        //消息持久化助手;
        MessageHelper messageHelper = imConfig.getMessageHelper();
        User user = imSessionContext.getClient().getUser();
        String userId = user.getId();
        String groupid = userReqBody.getGroup_id() != null ? userReqBody.getGroup_id() : "1";
        Group group = groupCache.get(groupid + SUBFIX + INFO, Group.class);
        if (group != null) {
            List<User> allUsers = ImAio.getAllOnlineUser();
            List<String> userIds = new ArrayList<>(allUsers.size());
            for (User u : allUsers) {
                userIds.add(u.getId());
            }
            List<User> users = new ArrayList<User>();

            Map<String, String> userInfo = new HashMap<>();
            Map<String, String> userInfoKeys = new HashMap<>();
            for (String userid : userIds) {
                userInfoKeys.put(userid, USER + SUBFIX + userid + SUBFIX + INFO);
            }
            try {
                userInfo = JedisTemplate.me().batchGetKeysToKeyMap(userInfoKeys);
                for (Map.Entry<String, String> entry : userInfo.entrySet()) {
                    User u = JSON.parseObject(entry.getValue(), User.class);
                    users.add(u);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            group.setUsers(users);
        }
        user = new User();
        user.setId(userId);
        List<Group> groupList = new ArrayList<>(1);
        groupList.add(group);
        user.setGroups(groupList);

        RespBody resPacket = resPacket = new RespBody(Command.COMMAND_GET_USER_RESP, ImStatus.C10005);
//        if (user == null) {
//            return ImKit.ConvertRespPacket(new RespBody(Command.COMMAND_GET_USER_RESP, ImStatus.C10004), channelContext);
//        }
        resPacket.setData(user);

//        end = System.currentTimeMillis();
//        System.out.println("获取用户信息 .. All Time:" + (end - start) + "(ms)");
        return ImKit.ConvertRespPacket(resPacket, channelContext);
    }

    private ImPacket dohandler(ImPacket packet, ChannelContext channelContext) throws Exception {
        long start, end;
        start = System.currentTimeMillis();

        UserReqBody userReqBody = JsonKit.toBean(packet.getBody(), UserReqBody.class);
        RespBody resPacket = null;
        ImSessionContext imSessionContext = (ImSessionContext) channelContext.getAttribute();
        User user = imSessionContext.getClient().getUser();
        String userId = user.getId();
        if (StringUtils.isEmpty(userId)) {
            return ImKit.ConvertRespPacket(new RespBody(Command.COMMAND_GET_USER_RESP, ImStatus.C10004), channelContext);
        }
        //(0:所有在线用户,1:所有离线用户,2:所有用户[在线+离线]);
        Integer type = userReqBody.getType() == null ? 2 : userReqBody.getType();
        String groupid = userReqBody.getGroup_id();
        user = getUserInfo(userId, type, groupid);

        List<String> manager = null;
        try {
            manager = groupCache.listGetAll(userReqBody.getGroup_id() + ":" + ImConst.MANAGER);
        } catch (Exception e) {
        }
        if (manager != null) {
            user.getGroups().get(0).setManager(manager);
        }
        //在线用户
        if (0 == userReqBody.getType()) {
            resPacket = new RespBody(Command.COMMAND_GET_USER_RESP, ImStatus.C10005);
            //离线用户;
        } else if (1 == userReqBody.getType()) {
            resPacket = new RespBody(Command.COMMAND_GET_USER_RESP, ImStatus.C10006);
            //在线+离线用户;
        } else if (2 == userReqBody.getType()) {
            resPacket = new RespBody(Command.COMMAND_GET_USER_RESP, ImStatus.C10003);
        }
        if (user == null) {
            return ImKit.ConvertRespPacket(new RespBody(Command.COMMAND_GET_USER_RESP, ImStatus.C10004), channelContext);
        }
        resPacket.setData(user);

        end = System.currentTimeMillis();
        System.out.println("获取用户信息 .. Run Time:" + (end - start) + "(ms)");
        return ImKit.ConvertRespPacket(resPacket, channelContext);
    }

    /**
     * 根据用户id获取用户在线及离线用户;
     * @param userid
     * @param type(0:所有在线用户,1:所有离线用户,2:所有用户[在线+离线])
     * @return
     */
    public User getUserInfo(String userid, Integer type, String groupid) {
        User user = null;
        //是否开启持久化;
        boolean isStore = ImConst.ON.equals(imConfig.getIsStore());
        //消息持久化助手;
        MessageHelper messageHelper = imConfig.getMessageHelper();
        if (isStore) {
            user = messageHelper.getUserByType(userid, 2);
            if (user == null) {
                return null;
            }
            if (groupid == null) {
                user.setFriends(messageHelper.getAllFriendUsers(userid, type));
                user.setGroups(messageHelper.getAllGroupUsers(userid, type));
            } else {
                user = new User();
                user.setId(userid);
                List<Group> groupList = new ArrayList<>(1);
                Group group = messageHelper.getGroupUsers(groupid, type);
                groupList.add(group);
                user.setGroups(groupList);
            }
            return user;
        } else {
            user = ImAio.getUser(userid);
            if (user == null) {
                return null;
            }
            User copyUser = ImKit.copyUserWithoutFriendsGroups(user);
            //在线用户;
            if (type == 0 || type == 1) {
                //处理好友分组在线用户相关信息;
                List<Group> friends = user.getFriends();
                List<Group> onlineFriends = initOnlineUserFriendsGroups(friends, type, 0);
                if (onlineFriends != null) {
                    copyUser.setFriends(onlineFriends);
                }
                //处理群组在线用户相关信息;
                List<Group> groups = user.getGroups();
                List<Group> onlineGroups = initOnlineUserFriendsGroups(groups, type, 1);
                if (onlineGroups != null) {
                    copyUser.setGroups(onlineGroups);
                }
                return copyUser;
                //所有用户(在线+离线);
            } else if (type == 2) {
                return user;
            }
        }
        return user;
    }

    /**
     * 处理在线用户好友及群组用户;
     * @param groups
     * @param flag(0：好友,1:群组)
     * @return
     */
    private static List<Group> initOnlineUserFriendsGroups(List<Group> groups, Integer type, Integer flag) {
        if (groups == null || groups.isEmpty()) {
            return null;
        }
        //处理好友分组在线用户相关信息;
        List<Group> onlineGroups = new ArrayList<Group>();
        for (Group group : groups) {
            Group copyGroup = ImKit.copyGroupWithoutUsers(group);
            List<User> users = null;
            if (flag == 1) {
                users = ImAio.getAllUserByGroup(group.getGroup_id());
            } else if (flag == 0) {
                users = group.getUsers();
            }
            if (users != null && !users.isEmpty()) {
                List<User> copyUsers = new ArrayList<User>();
                for (User userObj : users) {
                    User onlineUser = ImAio.getUser(userObj.getId());
                    //在线
                    if (onlineUser != null && type == 0) {
                        User copyOnlineUser = ImKit.copyUserWithoutFriendsGroups(onlineUser);
                        copyUsers.add(copyOnlineUser);
                        //离线
                    } else if (onlineUser == null && type == 1) {
                        User copyOnlineUser = ImKit.copyUserWithoutFriendsGroups(onlineUser);
                        copyUsers.add(copyOnlineUser);
                    }
                }
                copyGroup.setUsers(copyUsers);
            }
            onlineGroups.add(copyGroup);
        }
        return onlineGroups;
    }

    @Override
    public Command command() {
        return Command.COMMAND_GET_USER_REQ;
    }

}
