package org.jim.server.command.handler;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jim.common.ImAio;
import org.jim.common.ImConst;
import org.jim.common.ImPacket;
import org.jim.common.ImSessionContext;
import org.jim.common.ImStatus;
import org.jim.common.cache.redis.RedisCache;
import org.jim.common.cache.redis.RedisCacheManager;
import org.jim.common.packets.*;
import org.jim.server.command.handler.processor.group.GroupCmdProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tio.core.Aio;
import org.tio.core.ChannelContext;
import org.jim.common.utils.ImKit;
import org.jim.common.utils.JsonKit;
import org.jim.server.command.AbstractCmdHandler;

import java.util.Date;
import java.util.List;

/**
 * 版本: [1.0]
 * 功能说明: 加入群组消息cmd命令处理器
 *
 * @author : WChao 创建时间: 2017年9月21日 下午3:33:23
 */
public class JoinGroupReqHandler extends AbstractCmdHandler {

    private static Logger log = LoggerFactory.getLogger(JoinGroupReqHandler.class);
    private final String SUBFIX = ":";

    @Override
    public ImPacket handler(ImPacket packet, ChannelContext channelContext) throws Exception {
        JoinGroupResBody joinGroupNotifyRespBody = null;
        try {
            joinGroupNotifyRespBody = JsonKit.toBean(packet.getBody(), JoinGroupResBody.class);
        } catch (Exception e) {
            return getMessageFailedPacket(channelContext);
        }
        String groupid = joinGroupNotifyRespBody.getGroupId();
        ImSessionContext imSessionContext = (ImSessionContext) channelContext.getAttribute();
        User user = imSessionContext.getClient().getUser();
        JoinGroupRespMsgBody Body = new JoinGroupRespMsgBody(Command.COMMAND_JOIN_GROUP_RESP, user.getId(), user.getNick(), user.getAvatar(), groupid,
                new Date().getTime());
//        System.out.println("Body.toString():" + Body.toString());
        ImPacket imPacket = new ImPacket(Command.COMMAND_JOIN_GROUP_RESP, Body.toByte());
        ImAio.sendToGroup(groupid, imPacket);
        return null;
    }


    /**
     * 获取用户消息失败响应包;
     *
     * @param channelContext
     * @return
     */
    public ImPacket getMessageFailedPacket(ChannelContext channelContext) {
        RespBody resPacket = new RespBody(Command.COMMAND_JOIN_GROUP_RESP, ImStatus.C10022);
        return ImKit.ConvertRespPacket(resPacket, channelContext);
    }

    /**
     * 发送进房间通知;
     *
     * @param packet
     * @param channelContext
     */
    public void joinGroupNotify(ImPacket packet, ChannelContext channelContext) {
        ImSessionContext imSessionContext = (ImSessionContext) channelContext.getAttribute();

        User clientUser = imSessionContext.getClient().getUser();
        User notifyUser = clientUser;//new User(clientUser.getId(),clientUser.getNick());
        clientUser.setFriends(null);
        clientUser.setGroups(null);

        Group joinGroup = JsonKit.toBean(packet.getBody(), Group.class);
        String groupId = joinGroup.getGroup_id();
        /////add by george 保存禁言标志
		/*RedisCache groupCache = RedisCacheManager.getCache(ImConst.GROUP);
		Integer bannedInd = groupCache.get(groupId+SUBFIX+ "banned", Integer.class);
		if( bannedInd!=null ) {
		}else {
			groupCache.put(groupId+SUBFIX+ "banned", 0);
		}*/
        /////end  保存禁言标志

        //发进房间通知  COMMAND_JOIN_GROUP_NOTIFY_RESP
//		JoinGroupNotifyRespBody joinGroupNotifyRespBody = new JoinGroupNotifyRespBody().setGroup(groupId).setUser(notifyUser);
//		RespBody notifyRespBody = new RespBody(Command.COMMAND_JOIN_GROUP_NOTIFY_RESP,joinGroupNotifyRespBody);
//
//		ImPacket joinGroupNotifyrespPacket = new ImPacket(Command.COMMAND_JOIN_GROUP_NOTIFY_RESP,notifyRespBody.toByte());
//		ImAio.sendToGroup(groupId, joinGroupNotifyrespPacket);
    }

    /**
     * 绑定群组
     *
     * @param packet
     * @param channelContext
     * @return
     * @throws Exception
     */
    public ImPacket bindGroup(ImPacket packet, ChannelContext channelContext) throws Exception {
        if (packet.getBody() == null) {
            throw new Exception("body is null");
        }
        Group joinGroup = JsonKit.toBean(packet.getBody(), Group.class);
        String groupId = joinGroup.getGroup_id();
        if (StringUtils.isBlank(groupId)) {
            log.error("group is null,{}", channelContext);
            Aio.close(channelContext, "group is null when join group");
            return null;
        }
        //实际绑定之前执行处理器动作
        List<GroupCmdProcessor> groupCmdProcessors = this.getProcessor(channelContext, GroupCmdProcessor.class);

        //先定义为操作成功
        JoinGroupResult joinGroupResult = JoinGroupResult.JOIN_GROUP_RESULT_OK;
        JoinGroupRespBody joinGroupRespBody = new JoinGroupRespBody();
        //当有群组处理器时候才会去处理
        if (CollectionUtils.isNotEmpty(groupCmdProcessors)) {
            GroupCmdProcessor groupCmdProcessor = groupCmdProcessors.get(0);
            joinGroupRespBody = groupCmdProcessor.join(joinGroup, channelContext);
            if (joinGroupRespBody == null || JoinGroupResult.JOIN_GROUP_RESULT_OK.getNumber() != joinGroupRespBody.getResult().getNumber()) {
                RespBody joinRespBody = new RespBody(Command.COMMAND_JOIN_GROUP_RESP, ImStatus.C10012).setData(joinGroupRespBody);
                ImPacket respPacket = ImKit.ConvertRespPacket(joinRespBody, channelContext);
                return respPacket;
            }
        }
        //处理完处理器内容后
        ImAio.bindGroup(channelContext, groupId, imConfig.getMessageHelper().getBindListener());

        //回一条消息，告诉对方进群结果
        joinGroupRespBody.setGroup(groupId);
        ImSessionContext imSessionContext = (ImSessionContext) channelContext.getAttribute();
        Client client = imSessionContext.getClient();
        User clientUser = client.getUser();
//        User notifyUser = new User(clientUser.getId(), clientUser.getNick());
        // Alex20191108:用户进群返回用户基本信息中，增加头像、userType和账户名等信息---用于优化更新前端左侧的用户列表
        User notifyUser = new User();
        notifyUser.setId(clientUser.getId());
        notifyUser.setNick(clientUser.getNick());
        notifyUser.setAvatar(clientUser.getAvatar() != null ? clientUser.getAvatar() : null);
        notifyUser.setAccount(clientUser.getAccount() != null ? clientUser.getAccount() : clientUser.getId());
        notifyUser.setStatus(clientUser.getStatus());
        notifyUser.setUserType(clientUser.getUserType());
        notifyUser.setTerminal(clientUser.getTerminal());

        joinGroupRespBody.setUser(notifyUser);
        RespBody joinRespBody = new RespBody(Command.COMMAND_JOIN_GROUP_RESP, ImStatus.C10011).setData(joinGroupRespBody);
        ImPacket respPacket = ImKit.ConvertRespPacket(joinRespBody, channelContext);
        ImAio.sendToGroup(groupId, respPacket);
        return respPacket;
    }

    @Override
    public Command command() {

        return Command.COMMAND_JOIN_GROUP_REQ;
    }
}
