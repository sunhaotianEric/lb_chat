package org.jim.server.command.handler.processor.flushuser;

import org.jim.common.packets.User;
import org.jim.server.command.handler.processor.CmdProcessor;

public interface FlushUserServiceProcessor extends CmdProcessor {

    User flushUser(String id);

    String changeManager(String userid,String groupid);

}
