package org.jim.server.command.handler;

import org.jim.common.*;
import org.jim.common.packets.*;
import org.jim.common.utils.ImKit;
import org.jim.common.utils.JsonKit;
import org.jim.server.command.AbstractCmdHandler;
import org.jim.server.command.handler.processor.flushuser.FlushUserServiceProcessor;
import org.tio.core.ChannelContext;

import java.util.List;

public class FlushUserReqHandler extends AbstractCmdHandler {

    @Override
    public ImPacket handler(ImPacket packet, ChannelContext channelContext) throws Exception {
        FlushUserReqBody flushUserReqBody = null;
        try {
            flushUserReqBody = JsonKit.toBean(packet.getBody(), FlushUserReqBody.class);
        } catch (Exception e) {
            //用户消息格式不正确
            return getMessageFailedPacket(channelContext);
        }
        List<FlushUserServiceProcessor> friendCmdProcessors = this.getProcessor(channelContext, FlushUserServiceProcessor.class);
        FlushUserServiceProcessor flushUserServiceProcessor = friendCmdProcessors.get(0);
        Integer cmdType = flushUserReqBody.getCmdType();
        ImSessionContext imSessionContext = (ImSessionContext) channelContext.getAttribute();
        User user = imSessionContext.getClient().getUser();
        switch (cmdType) {
            case 1:
                //更新用户信息
                User usertow = flushUserServiceProcessor.flushUser(user.getId());
                imSessionContext.getClient().setUser(usertow);
//                // Alex20191108影响性能暂无必要
                String groupId = "1";
                if (user.getGroups() != null && user.getGroups().get(0) != null) {
                    groupId = user.getGroups().get(0).getGroup_id();
                }
                RespBody rb = new RespBody(Command.COMMAND_FLUSHUSER_RESP, ImStatus.C10003).setData(setUser(usertow));
                ImPacket chatPacket = new ImPacket(Command.COMMAND_FLUSHUSER_RESP, rb.toByte());
                ImAio.sendToGroup(groupId, chatPacket); // 群发消息
                return ImKit.ConvertRespPacket(new RespBody(Command.COMMAND_FLUSHUSER_RESP, ImStatus.C10000).setData(setUser(usertow)), channelContext);

            case 2:
                //群管理变动更新
                String groupid = String.valueOf(flushUserReqBody.getExtras().get("groupId"));
                if (groupid != null) {
                    flushUserServiceProcessor.changeManager(user.getId(), groupid);
                }
                break;
        }
        return ImKit.ConvertRespPacket(new RespBody(Command.COMMAND_FLUSHUSER_RESP, ImStatus.C10000), channelContext);
    }

    private User setUser(User clientUser) {
        User notifyUser = new User();
        notifyUser.setId(clientUser.getId());
        notifyUser.setNick(clientUser.getNick());
        notifyUser.setAvatar(clientUser.getAvatar() != null ? clientUser.getAvatar() : null);
        notifyUser.setAccount(clientUser.getAccount() != null ? clientUser.getAccount() : clientUser.getId());
        notifyUser.setStatus(clientUser.getStatus());
        notifyUser.setUserType(clientUser.getUserType());
        notifyUser.setTerminal(clientUser.getTerminal());
        return notifyUser;
    }

    @Override
    public Command command() {
        return Command.COMMAND_FLUSHUSER_REQ;
    }

    /**
     * 获取用户消息失败响应包;
     *
     * @param channelContext
     * @return
     */
    public ImPacket getMessageFailedPacket(ChannelContext channelContext) {
        RespBody resPacket = new RespBody(Command.COMMAND_ADDFRIEND_RESP, ImStatus.C10022);
        return ImKit.ConvertRespPacket(resPacket, channelContext);
    }
}
