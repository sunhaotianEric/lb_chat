package org.jim.server.command.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.jim.common.ImAio;
import org.jim.common.ImConst;
import org.jim.common.ImPacket;
import org.jim.common.ImStatus;
import org.jim.common.cache.redis.RedisCache;
import org.jim.common.packets.*;
import org.jim.common.utils.ChatKit;
import org.jim.common.utils.ImKit;
import org.jim.common.utils.JsonKit;
import org.jim.server.command.AbstractCmdHandler;
import org.jim.server.command.handler.processor.chat.MsgQueueRunnable;
import org.tio.core.ChannelContext;

import java.util.List;

/**
 * 功能说明: 处理群聊审核消息结果处理器
 *
 * @author : Alex 创建时间: 2019年10月12日 下午7:00:10
 */
public class ChatCheckedReqHandler extends AbstractCmdHandler {

    RedisCache checkChatCache = new RedisCache(ImConst.CHECKCHAT, -1, -1);

    @Override
    public ImPacket handler(ImPacket packet, ChannelContext channelContext) throws Exception {
        MessageReqBody messageReqBody = null;
        try {
            messageReqBody = JsonKit.toBean(packet.getBody(), MessageReqBody.class);
        } catch (Exception e) {
            //用户消息格式不正确
            return getMessageFailedPacket(channelContext);
        }
        ImPacket chatPacket = null;
        ChatBody chatBody = ChatKit.toChatBody(packet.getBody(), channelContext); // JsonKit.toBean(packet.getBody(), ChatBody.class)
        packet.setBody(JSON.toJSONString(chatBody).getBytes());
        String groupId = chatBody.getGroup_id();
        String key = "group:" + chatBody.getGroup_id();
        List msg = checkChatCache.sortSetGetAll(key, chatBody.getCreateTime(), chatBody.getCreateTime());
        if (msg == null || msg.size() == 0){
//            System.out.println("// 此条消息已审核过了，缓存中已不存在");
            RespBody resPacket = new RespBody(Command.COMMAND_CHATCHECKED_RESP, ImStatus.C10034);
            return ImKit.ConvertRespPacket(resPacket, channelContext);
        }
        if (chatBody.getVerification().equals(1)) { //群聊信息审核通过
            chatBody.setCmd(Command.COMMAND_CHAT_REQ.getNumber());
            chatPacket = new ImPacket(Command.COMMAND_CHAT_REQ, new RespBody(Command.COMMAND_CHAT_REQ, chatBody).toByte());
            ImAio.sendToGroup(groupId, chatPacket); // "转发"群聊消息
            //异步调用业务处理消息接口(存入群组历史消息)
            if (ChatType.forNumber(chatBody.getChatType()) != null) {
                MsgQueueRunnable msgQueueRunnable = (MsgQueueRunnable) channelContext.getAttribute(ImConst.CHAT_QUEUE);
                msgQueueRunnable.addMsg(packet);
                msgQueueRunnable.getExecutor().execute(msgQueueRunnable);
            }
        } else if (chatBody.getVerification().equals(-1)) { //群聊信息审核失败

        }
        // 从“未审核历史消息列表”缓存中删除此条消息
//        checkChatCache.remove(key + ":" + chatBody.getCreateTime().toString());
        new Thread() {
            public void run() { // 异步处理
                checkChatCache.sortSetRemove(key, chatBody.getCreateTime(), chatBody.getCreateTime());
            }
        }.start();
        RespBody resPacket = new RespBody(Command.COMMAND_CHATCHECKED_RESP, ImStatus.C10032);
        return ImKit.ConvertRespPacket(resPacket, channelContext);
    }

    /**
     * 获取用户消息失败响应包;
     *
     * @param channelContext
     * @return
     */
    public ImPacket getMessageFailedPacket(ChannelContext channelContext) {
        RespBody resPacket = new RespBody(Command.COMMAND_CHATCHECKED_RESP, ImStatus.C10022);
        return ImKit.ConvertRespPacket(resPacket, channelContext);
    }

    @Override
    public Command command() {
        return Command.COMMAND_CHATCHECKED_REQ; // 43:群聊审核结果请求
    }
}
