package org.jim.server.command.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jfinal.plugin.redis.Redis;
import org.jim.common.ImAio;
import org.jim.common.ImConst;
import org.jim.common.ImPacket;
import org.jim.common.ImStatus;
import org.jim.common.cache.redis.RedisCache;
import org.jim.common.cache.redis.RedisCacheManager;
import org.jim.common.packets.*;
import org.jim.common.utils.ChatKit;
import org.jim.common.utils.ImKit;
import org.jim.common.utils.JsonKit;
import org.jim.server.command.AbstractCmdHandler;
import org.jim.server.entity.UserMessgeEntity;
import org.jim.server.util.HttpRestClient;
import org.tio.core.ChannelContext;
import sun.plugin2.message.Serializer;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import static com.alibaba.fastjson.JSON.toJSONString;

/**
 * 功能说明: 获取用户群聊审核消息处理器
 *
 * @author : Alex 创建时间: 2019年10月12日 下午6:58:59
 */
public class ChatCheckingReqHandler extends AbstractCmdHandler {

    RedisCache checkChatCache = new RedisCache(ImConst.CHECKCHAT, 10, -1);

    RedisCache userCache = new RedisCache(ImConst.USER, -1, -1);

    RedisCache groupCache = RedisCacheManager.getCache(ImConst.GROUP);

    HttpRestClient httpRestClient=new HttpRestClient();

    @Override
    public ImPacket handler(ImPacket packet, ChannelContext channelContext) throws Exception {

        String  remoteAddress=String.valueOf(channelContext.getAsynchronousSocketChannel().getRemoteAddress());
        String [] remotes = remoteAddress.split(":");
        String ipAddress = remotes[0].replace("/","");
        UserMessgeEntity userMessgeEntity =groupCache.get("userIP:"+ipAddress, UserMessgeEntity.class);
        try{
            if(userMessgeEntity.getFlag()==1){
                return getMessageFailedPacket(channelContext);
            }
        }catch (Exception e){
            //不影响
        }


        MessageReqBody messageReqBody = null;
        try {
            messageReqBody = JsonKit.toBean(packet.getBody(), MessageReqBody.class);
        } catch (Exception e) {
            //用户消息格式不正确
            return getMessageFailedPacket(channelContext);
        }

        ChatBody chatBody = ChatKit.toChatBody(packet.getBody(), channelContext); // JsonKit.toBean(packet.getBody(), ChatBody.class)
        this.setFromUserinfo(chatBody); // 将redis缓存中的用户昵称和头像等详细信息附加到消息包中
        chatBody.setVerification(0); // 设置审核参数
        packet.setBody(toJSONString(chatBody).getBytes());
//        System.out.println(chatBody.toJsonString());
        ImPacket checkchatPacket = new ImPacket(Command.COMMAND_CHATCHECKING_REQ, new RespBody(Command.COMMAND_CHATCHECKING_REQ, chatBody).toByte());
        //设置同步序列号;
        checkchatPacket.setSynSeq(packet.getSynSeq());
        String groupId = chatBody.getGroup_id();
        ImAio.sendToGroup(groupId, checkchatPacket);

        // 未审核消息写入缓存“未审核历史消息列表”中
        new Thread() {
            public void run() { // 异步处理
//                checkChatCache.put(chatBody.getGroup_id() + ":" + chatBody.getId(), chatBody, 43200); // 缓存12小时时间
                String key = "group:" + chatBody.getGroup_id();
                checkChatCache.sortSetPush(key, chatBody.getCreateTime(), chatBody);
                try {
                    UserMessgeEntity entity = new UserMessgeEntity(ipAddress, String.valueOf(channelContext.getUserid()),
                            chatBody.getContent(), 0, new Date());
                    groupCache.put("userIP:"+ipAddress,entity);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }.start();
        RespBody resPacket = new RespBody(Command.COMMAND_CHATCHECKING_RESP, ImStatus.C10031);
        resPacket.setData(chatBody);
        return ImKit.ConvertRespPacket(resPacket, channelContext);
    }

    private Boolean setFromUserinfo(ChatBody chatBody) {
        String userid = chatBody.getFrom();
        if (userCache.get(userid + ":" + ImConst.INFO) != null) {
            User user = userCache.get(userid + ":" + ImConst.INFO, User.class);
            chatBody.setFromName(user.getAccount() != null ? user.getAccount() : user.getNick());
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("fromAvatar", user.getAvatar());
            jsonObj.put("fromNick", user.getNick());
            chatBody.setExtras(jsonObj);
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取用户消息失败响应包;
     *
     * @param channelContext
     * @return
     */
    public ImPacket getMessageFailedPacket(ChannelContext channelContext) {
        RespBody resPacket = new RespBody(Command.COMMAND_CHATCHECKHIS_RESP, ImStatus.C10022);
        return ImKit.ConvertRespPacket(resPacket, channelContext);
    }

    @Override
    public Command command() {
        return Command.COMMAND_CHATCHECKING_REQ; // 42:群聊审核验证请求
    }
}
