package org.jim.server.command.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;
import org.jim.common.*;
import org.jim.common.cache.redis.RedisCache;
import org.jim.common.cache.redis.RedisCacheManager;
import org.jim.common.packets.*;
import org.jim.common.utils.ChatKit;
import org.jim.common.utils.ImKit;
import org.jim.server.command.AbstractCmdHandler;
import org.jim.server.command.handler.processor.chat.ChatCmdProcessor;
import org.jim.server.command.handler.processor.chat.MsgQueueRunnable;
import org.tio.core.ChannelContext;
import org.tio.utils.lock.SetWithLock;

import java.util.ArrayList;
import java.util.List;

/**
 * 版本: [1.0]
 * 功能说明: 聊天请求cmd消息命令处理器
 *
 * @author : Alex 创建时间: 2019年10月12日 下午5:52:19
 */
public class ChatReqHandler extends AbstractCmdHandler {

    RedisCache userCache = new RedisCache(ImConst.USER, -1, -1);

    @Override
    public ImPacket handler(ImPacket packet, ChannelContext channelContext) throws Exception {
        if (packet.getBody() == null) {
            throw new Exception("body is null");
        }
        ChatBody chatBody = ChatKit.toChatBody(packet.getBody(), channelContext);
        this.setFromUserinfo(chatBody);
//        packet.setBody(chatBody.toByte());
        packet.setBody(JSON.toJSONString(chatBody).getBytes());
        //聊天数据格式不正确
        if (chatBody == null || chatBody.getChatType() == null) {
            ImPacket respChatPacket = ChatKit.dataInCorrectRespPacket(channelContext);
            return respChatPacket;
        }
        List<ChatCmdProcessor> chatProcessors = this.getProcessorNotEqualName(Sets.newHashSet(ImConst.BASE_ASYNC_CHAT_MESSAGE_PROCESSOR), ChatCmdProcessor.class);
        if (CollectionUtils.isNotEmpty(chatProcessors)) {
            chatProcessors.forEach(chatProcessor -> chatProcessor.handler(packet, channelContext));
        }
        //异步调用业务处理消息接口
        if (ChatType.forNumber(chatBody.getChatType()) != null) {
            MsgQueueRunnable msgQueueRunnable = (MsgQueueRunnable) channelContext.getAttribute(ImConst.CHAT_QUEUE);
            msgQueueRunnable.addMsg(packet);
            msgQueueRunnable.getExecutor().execute(msgQueueRunnable);
        }
        ImPacket chatPacket = new ImPacket(Command.COMMAND_CHAT_REQ, new RespBody(Command.COMMAND_CHAT_REQ, chatBody).toByte());
        //设置同步序列号;
        chatPacket.setSynSeq(packet.getSynSeq());
        //私聊
        if (ChatType.CHAT_TYPE_PRIVATE.getNumber() == chatBody.getChatType()) {
            String toId = chatBody.getTo();
            if (ChatKit.isOnline(toId, imConfig)) {
                ImAio.sendToUser(toId, chatPacket);
                //发送成功响应包
                return ChatKit.sendSuccessRespPacket(channelContext);
            } else {
                //用户不在线响应包
                return ChatKit.offlineRespPacket(channelContext);
            }
            //群聊
        } else if (ChatType.CHAT_TYPE_PUBLIC.getNumber() == chatBody.getChatType()) {
            String group_id = chatBody.getGroup_id();
            ImAio.sendToGroup(group_id, chatPacket);
            //发送成功响应包
            return ChatKit.sendSuccessRespPacket(channelContext);
        }
        return null;
    }

    private Boolean setFromUserinfo(ChatBody chatBody) {
        String userid = chatBody.getFrom();
        if (userCache.get(userid + ":" + ImConst.INFO) != null) {
            User user = userCache.get(userid + ":" + ImConst.INFO, User.class);
            chatBody.setFromName(user.getAccount());
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("fromAvatar", user.getAvatar());
            jsonObj.put("fromNick", user.getNick());
//            jsonObj.put("fromUserAccount",user.getAccount());
            chatBody.setExtras(jsonObj);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Command command() {
        return Command.COMMAND_CHAT_REQ;
    }
}
