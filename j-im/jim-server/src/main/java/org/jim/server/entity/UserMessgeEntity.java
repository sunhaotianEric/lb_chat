package org.jim.server.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class UserMessgeEntity implements Serializable{

    String ip;

    String userId;

    String lastSendMsg;

    Integer flag;

    Date lastSendTime;

    public UserMessgeEntity(String ip, String userId, String lastSendMsg, Integer flag, Date lastSendTime) {
        this.ip = ip;
        this.userId = userId;
        this.lastSendMsg = lastSendMsg;
        this.flag = flag;
        this.lastSendTime = lastSendTime;
    }
}
