package org.jim.server.command.handler;

import org.jim.common.ImAio;
import org.jim.common.ImConst;
import org.jim.common.ImPacket;
import org.jim.common.ImStatus;
import org.jim.common.cache.redis.RedisCache;
import org.jim.common.cache.redis.RedisCacheManager;
import org.jim.common.packets.BannedBody;
import org.jim.common.packets.CloseReqBody;
import org.jim.common.packets.Command;
import org.jim.common.packets.Message;
import org.jim.common.packets.RespBody;
import org.jim.common.utils.ImKit;
import org.jim.common.utils.JsonKit;
import org.jim.server.command.AbstractCmdHandler;
import org.tio.core.ChannelContext;

public class BannedReqHandler extends AbstractCmdHandler {

    @Override
    public ImPacket handler(ImPacket packet, ChannelContext channelContext) throws Exception {
        BannedBody bannedBody = null;
        try {
            bannedBody = JsonKit.toBean(packet.getBody(), BannedBody.class);
        } catch (Exception e) {
            //禁言设置请求消息格式不正确
            return ImKit.ConvertRespPacket(new RespBody(Command.COMMAND_BANNED_RESP, ImStatus.C10022), channelContext);
        }

        String groupId = bannedBody.getGroupId();
        /////add by george 保存禁言标志
        RedisCache groupCache = RedisCacheManager.getCache(ImConst.GROUP);
        groupCache.put(groupId + ":setting:banned", bannedBody.getBannedInd());
        /////end  保存禁言标志

        RespBody resPacket = new RespBody(Command.COMMAND_BANNED_RESP, ImStatus.C10040);
        resPacket.setData(bannedBody);
        ImPacket chatPacket = ImKit.ConvertRespPacket(resPacket, channelContext);
        ImAio.sendToGroup(groupId, chatPacket);

        return null;//ImKit.ConvertRespPacket(resPacket, channelContext);
    }

    @Override
    public Command command() {
        return Command.COMMAND_BANNED_REQ;
    }
}
