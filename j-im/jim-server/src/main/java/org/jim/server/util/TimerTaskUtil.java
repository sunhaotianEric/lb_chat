package org.jim.server.util;

import org.jim.common.ImConst;
import org.jim.common.cache.redis.RedisCache;

import java.util.*;

public class TimerTaskUtil {
//    public static void main(String[] args) {
//        timerEveryDay();
//        timer4Hours();
//    }

    public static void start() {
        timer4Hours();
//        timerEveryDay();
    }

    /**
     * 设置每天00:00:00和12:00:00执行任务：清除12小时之前的历史聊天记录
     * java.util.Timer.scheduleAtFixedRate(TimerTask task, Date firstTime, long period)
     */
    public static void timerEveryDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 5);
        calendar.set(Calendar.SECOND, 0);

        Date time = calendar.getTime();

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                System.out.println("-------每天00:00:00和12:00:00执行‘清除12小时之前的历史聊天记录’任务--------");
                clearStoreHis();
            }
        }, time, 1000 * 60 * 60 * 12);// 这里设定将延时每12小时固定执行
    }

    public static void clearStoreHis() {
        RedisCache storeCache = new RedisCache(ImConst.STORE, -1, -1);
        Set<String> storeGroups = storeCache.keys("store:group");
        long intervalTime = 1000 * 60 * 60 * 12;
        double end = new Date().getTime() - intervalTime;
        for (String key : storeGroups) {
            String group = key.substring(key.indexOf(":") + 1);
            new Thread() {
                public void run() { // 异步处理
                    Long num = storeCache.sortSetRemove(group, 0, end);
                    System.out.println(new Date() + "移除Store记录数：" + num);
                }
            }.start();
        }
    }

    /**
     * 设置每隔4小时执行任务：清除四小时以前的历史审核记录
     */
    public static void timer4Hours() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 21);
        calendar.set(Calendar.MINUTE, 1);
        calendar.set(Calendar.SECOND, 0);

        Date time = calendar.getTime();

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                System.out.println("-------每隔4小时执行任务--------");
                clearCheckHis();
            }
        }, time, 1000 * 60 * 60 * 4);
    }

    public static void clearCheckHis() {
        RedisCache checkChatCache = new RedisCache(ImConst.CHECKCHAT, -1, -1);
        Set<String> checkChatGroups = checkChatCache.keys("checkchat:group");
//        System.out.println(checkChatGroups);
        long intervalTime = 1000 * 60 * 60 * 4;
        double end = new Date().getTime() - intervalTime;
        new Thread() {
            public void run() { // 异步处理
                Long num = checkChatCache.sortSetRemove("1", 0, end);
                System.out.println(new Date() + "移除checkChat记录数：" + num);
            }
        }.start();
//        for (String key : checkChatGroups) {
//            String group = key.substring(key.indexOf(":") + 1);
//        }
    }
}
